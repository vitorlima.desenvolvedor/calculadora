﻿namespace Calculadora.API.Enum
{
    public enum Operacao
    {
        Somar = 1,
        Subtrair,
        Multiplicar,
        Dividir
    }
}