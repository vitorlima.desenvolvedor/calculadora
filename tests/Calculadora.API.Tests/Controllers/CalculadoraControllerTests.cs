﻿using Calculadora.API.Controllers;
using Calculadora.API.Enum;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Calculadora.API.Tests.Controllers
{
    public class CalculadoraControllerTests
    {
        [Theory]
        [InlineData(0,0,0)]
        [InlineData(15,38,53)]
        [InlineData(30,-15,15)]
        [InlineData(-30,-15,-45)]
        [InlineData(-30.9d,-15,-45.9d)]
        public void Deve_Somar_Corretamente(double valor1,double valor2,double resultadoEsperado)
        {
            var controller = new CalculadoraController();

            var result = controller.CalculoBasico(valor1,valor2 , Operacao.Somar);
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult);
            Assert.Equal(resultadoEsperado.ToString(), okResult.Value);
        }

        [Fact]
        public void Deve_Retornar_Erro()
        {
            var controller = new CalculadoraController();

            var result = controller.CalculoBasico(10, 5, (Operacao)5);
            var badRequestResult = result as BadRequestObjectResult;

            Assert.NotNull(badRequestResult);
            Assert.Equal(CalculadoraResources.MensagemOperacaoInvalida,badRequestResult.Value);
        }
    }
}