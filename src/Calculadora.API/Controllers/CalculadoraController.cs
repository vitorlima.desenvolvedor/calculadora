﻿using Calculadora.API.Enum;
using Microsoft.AspNetCore.Mvc;

namespace Calculadora.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CalculadoraController : ControllerBase
    {

        [HttpGet]
        [Route("calculo-basico")]
        public IActionResult CalculoBasico(double valor1, double valor2, Operacao operacao)
        {
            var resultado = 0.00d;

            switch (operacao)
            {
                case Operacao.Somar:
                    resultado = valor1 + valor2;
                    break;
                case Operacao.Subtrair:
                    resultado = valor1 - valor2;
                    break;
                case Operacao.Multiplicar:
                    resultado = valor1 * valor2;
                    break;
                case Operacao.Dividir:
                    resultado = valor1 / valor2;
                    break;
                default:
                    return BadRequest(CalculadoraResources.MensagemOperacaoInvalida);
            }


            return Ok(resultado.ToString());

        }
    }
}